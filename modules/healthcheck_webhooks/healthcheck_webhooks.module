<?php

/**
 * @file
 * Contains healthcheck_webhooks.module.
 */

use \Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_form_FORM_ID_alter().
 */
function healthcheck_webhooks_form_healthcheck_settings_form_alter(&$form,  FormStateInterface $form_state, $form_id) {
  $config = \Drupal::config('healthcheck_webhooks.settings');

  // Add settings for the Zapier webhook to the Healthcheck settings form.
  $form['webhooks'] = [
    '#type' => 'fieldset',
    '#title' => t('Webhooks integration'),
  ];

  $default_zapier = $config->get('zapier');
  $form['webhooks']['zapier'] = [
    '#type' => 'url',
    '#title' => t('Zapier URL'),
    '#description' => t('The URL of the Zapier webhook to which you want to send notifications.'),
    '#required' => FALSE,
    '#default_value' => empty($default_zapier) ? '' : $default_zapier,
  ];

  $form['actions']['submit']['#submit'][]  = 'healthcheck_webhooks_healthcheck_settings_form_submit';
}

/*
 * Submit handler for Healthcheck settings form.
 */
function healthcheck_webhooks_healthcheck_settings_form_submit($form, FormStateInterface $form_state) {
  // Save the Zapier url in the module config.
  $config = \Drupal::service('config.factory')->getEditable('healthcheck_webhooks.settings');

  $config->set('zapier', $form_state->getValue('zapier'))
    ->save();
}
