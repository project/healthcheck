# Healthcheck

Don't just hope your site is healthy, give it a healthcheck!

Healthcheck is like your site's own personal physician. The Healthcheck module runs in the background of your site and runs over a hundred periodic checks to ensure your site is secure, performing well and adhering to operations best practices. Healthcheck results are shown in a simple action-oriented dashboard so you know the health of your site at a glance.

## Installation

Install Healthcheck like you would any other Drupal 8 module:

1. Download the module using one of the following two methods:
   * Download the module to `<your_site>/modules`
   * Use Composer: `composer require drupal/healthcheck`
2. Enable the module under **Admin &gt; Extend**
3. Under **Admin &gt; People**, grant the following permissions as needed:
   * Access Healthcheck
   * Configure Healthcheck
   * View Healthcheck History

## Basic Use

By default, Healthcheck runs in "ad hoc mode," where reports are only run on demand.

To run an ad hoc healthcheck:


1. Log in as someone with **Access Healthcheck** permission.
2. Navigate to **Admin &gt; Reports &gt; Healthcheck**.

## Background reporting

The real power of Healthcheck is running new reports in the background. This allows your site to be constantly monitored for performance, usage and best practices.

To enable background reports:

1. Log in as someone with the **Configure Healthcheck** permission.
2. Navigate to **Admin &gt; Config &gt; System &gt; Healthcheck**.
3. Under **Background Processing**, select how often to run Healthcheck in the background.


## Enabling notifications

By default, Healthcheck will write to the results of background checks
to the Drupal log.

Healthcheck includes several modules that provide additional notification methods:

* **Healthcheck Email** sends background reports and critical findings to you via email.
* **Healthcheck Webhook** posts critical findings to a JSON endpoint such as Zapier. Zapier can then be configured to forward the message to a chat system like Slack.

To configure notifications:

1. Log in as someone with **Administer Site Configuration** permission.
2. Navigate to **Admin &gt; Extend**, and enable one or more of the above notification modules.
3. Navigate to **Admin &gt; Config &gt; System &gt; Healthcheck**.
4. Enable **Background processing**.
5. Configure notifications as needed.

## Historical reporting

To log how the health of your site changes over time, you can use the included **Healthcheck Historical** module. 

To enable historical reporting:

1. Log in as someone with **Administer Site Configuration** permission.
2. Navigate to **Admin &gt; Extend**, and enable **Healthcheck Historical**.
3. Navigate to **Admin &gt; Config &gt; System &gt; Healthcheck**.
4. Enable **Background processing**.
5. Under **Historical Reporting**, select how long reports should be retained.

To view historical reports:

1. Log in as someone with **View Healthcheck History** permission.
2. Navigate to **Admin &gt; Reports &gt; Healthcheck History**.
